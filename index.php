<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['save'])) {
      print('Спасибо, результаты сохранены.');
    }
    include('form.php');
    exit();
}

$result;

try{

    $errors = FALSE;
    if (empty($_POST['field-name'])) {
        print('Заполните имя.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['field-email'])) {
        print('Заполните почту.<br/>');
        $errors = TRUE;
    }

    if (empty($_POST['BIO'])) {
        print('Заполните биографию.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['ch'])) {
        print('Вы должны быть согласны с условиями.<br/>');
        $errors = TRUE;
    }

    if ($errors) {
        exit();
    }

    $sup= implode(",",$_POST['superpower']);

    $connection = new PDO("mysql:host=localhost;dbname=u20325", 'u20325', '1210789', array(PDO::ATTR_PERSISTENT => true));

    
    $user = $connection->prepare("INSERT INTO form SET name = ?, email = ?, bday = ?, sex = ?, limbs = ?, bio = ?, che = ?");
    $user -> execute([$_POST['field-name'], $_POST['field-email'], date('Y-m-d', strtotime($_POST['field-date'])), $_POST['radio-sex'], $_POST['radio-limb'], $_POST['BIO'], $_POST['ch']]);
    $id_user = $connection->lastInsertId();

    $user1 = $connection->prepare("INSERT INTO SUPER SET id = ?, super_name = ?");
    $user1 -> execute([$id_user, $sup]);
    $result = true;
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}


if ($result) {
  echo "Информация занесена в базу данных под ID №" . $id_user;
}
?>
